#include <string>

// this header exists solely to show the example function under test, feel free to remove it.
bool example_function_under_test();

std::string return_hello_world();